# Serverless Chess

Serverless Chess is a working project designed to showcase the potential for the
use of serverless architecture in not only chess, but any turn based game.

You can read more about the purpose behind this project on a blog post here.

---

## Setup

Serverless chess is built with SAM, so all you need to implement it into your AWS cloud is to run the commands:

	sam build
	sam deploy

After running `sam deploy`, check for the `WebSocketURI` output. This URL is needed for a client to connect to and interact with the game.

---

## Server API routes

When communication with the server, every message is a JSON string that must include an "action" key, which must have one of the following values:

* [action: create_game](#action-create_game)
* [action: join_game](#action-join_game)
* [action: make_move](#action-make_move)
* [action: send_chat](#action-send_chat)

### action: create_game

Request syntax:

```json
{
	"action": "create_game",
	"nickname": String
}
```

Parameters:

* nickname: (String) Nickname to be used in game, max 14 characters

After creating a game, the server will respond back with [event: game_created](#event-game_created).


### action: join_game

Request syntax:

```json
{
	"action": "join_game",
	"game_id": Number,
	"nickname": String,
	"token": Number
}
```

Parameters:

* nickname: (String) Nickname to be used in game, max 14 characters
* game_id: (Number) ID of the game to join
* *token*: (Number, optional) If rejoining a game, the token assigned

Upon successfully joining a game, the server will respond back with [event: game_join](#event-game_join), and will notify the opponent with [event: opp_join](#event:-opp_join)

If joining a game is unsuccessful, the server will respond back with [event: join_error](#event-join_error)

### action: make_move

Request syntax:

```json
{
	"action": "join_game",
	"token": Number,
	"game_id": Number,
	"move": String
}
```

Parameters:

* token: (Number) The token assigned at game creation or join
* game_id: (Number) The ID of the game
* move: (String) The move to make, in algebraic notation. Omit any appended notation, such as check, checkmate, or draw. The move supplied should be equal to one of the legal moves sent after the previous move.

Upon successfully making a move, the server will respond back to the player and opponent with [event: move](#event:-move).

If a move is invalid, or a move is made at an illegal time, the server will respond back with [event: move_error](#event:-move_error)

### action: send_chat

Request syntax:

```json
{
	"action": "join_game",
	"token": Number,
	"game_id": Number,
	"message": String
}
```

Parameters:

* token: (Number) The token assigned at game creation or join
* game_id: (Number) The ID of the game
* message: (String) The message to send to the opponent.

Upon successfully sending a chat message, the server will respond back with [event: chat](#event:-chat)

If an error occurs, the server will respond back with [event: chat_error](#event:-chat_error)

---

## Server API responses

Messages from the server will be in JSON format, and will always include an "event" key with one of the following values:

* [event: game_created](#event-game_created)
* [event: opp_join](#event-opp_join)
* [event: game_join](#event-game_join)
* [event: join_error](#event-join_error)
* [event: chat](#event-chat)
* [event: chat_error](#event-chat_error)
* [event: move](#event-move)
* [event: move_error](#event-move_error)
* [event: opp_disconnect](#event-opp_disconnect)

### event: game_created

Response syntax:

```json
{
	"event": "game_created",
	"game": {
		"game_id": Number,
		"active": Boolean,
		"moves": [String, ...],
		"legal_moves": [String, ...],
		"white_name": String,
		"black_name": String,
		"white_token": Number,
		"black_token": Number
	},
	"legal_moves": [String, ...]
}
```
Parameters:

* game: (Object) the game object
	* game_id: (Number) the ID of the game
	* active: (Boolean) If the game is still active or not
	* moves: (List of String) List of all the moves made so far, in algebraic notation
	* legal_moves: (List of String) List of legal moves in the current position
	* black_name: (String) Nickname for the black player
	* white_name: (String) Nickname for the white player
	* white_token: (String, conditional) If the player is white, the token needed to play the game
	* black_token: (String, conditional) If the player is black, the token needed to play the game
* legal_moves: (List of String) List of legal moves for the current turn. If it is not the player's turn, they will receive an empty list.

### event: opp_join

Response syntax:

```json
{
	"event": "opp_join",
	"opp_name": String
}
```

Parameters:

* opp_name: (String) The nickname of the opponent

If the opponent later disconnects and rejoins, the same event will be sent

Once the opponent joins, the white player can make their first legal move

### event: game_join

Response syntax:

```json
{
	"event": "game_created",
	"game": {
		"game_id": Number,
		"active": Boolean,
		"moves": [String, ...],
		"legal_moves": [String, ...],
		"white_name": String,
		"black_name": String,
		"white_token": Number,
		"black_token": Number
	},
	"legal_moves": [String, ...]
}
```
Parameters:

* game: (Object) the game object
	* game_id: (Number) the ID of the game
	* active: (Boolean) If the game is still active or not
	* moves: (List of String) List of all the moves made so far, in algebraic notation
	* legal_moves: (List of String) List of legal moves in the current position
	* black_name: (String) Nickname for the black player
	* white_name: (String) Nickname for the white player
	* white_token: (String, conditional) If the player is white, the token needed to play the game
	* black_token: (String, conditional) If the player is black, the token needed to play the game
* legal_moves: (List of String) List of legal moves for the current turn. If it is not the player's turn, they will receive an empty list.

Upon joining a game, the opponent will be notified with [event: opp_join](#event:-opp_join)

### event: join_error

Response syntax:

```json
{
	"event": "join_error",
	"message": String
}
```

Parameters:

* message: (String) Explanation for why the game cannot be joined

If the game is full (already has two players), the player will receive the message "game_full".

If the game ID cannot be found, the player will receive the message "game_not_found"

If a player is attempting to rejoin a game but does not have a valid token, they will receive the message "invalid_token"

### event: chat

Response syntax:

```json
{
	"event": "chat",
	"message": String,
	"from": String
}
```

Parameters:

* message: (String) The chat message sent.
* from: (String) The nickname of the player who sent the message

### event: chat_error

Response syntax:

```json
{
	"event": "chat_error",
	"message": String
}
```

Parameters:

* message: (String) The error message

If a player attempts to send a chat message to a game they are not authorized to, they will receive the message "not_allowed"

If a player sends a chat message but the opponent is currently disconnected or not there, they will receive the message "no_one_heard"

### event: move

Response syntax:

```json
{
	"event": "move",
	"move": String,
	"legal_moves": [String, ...]
}
```

Parameters:

* move: (String) The move made
* legal_moves: (List of String) The list of legal moves to be made next. If it is not the players turn, the player will receive an empty list.

If the move made results in a check, statemate, or checkmate, the proper symbol will be appended to it (+ for check, = for stalemate, # for checkmate).

### event: move_error

Response syntax:

```json
{
	"event": "move_error",
	"message": String
}
```

Parameters:

* message: (String) The error message

If a player provides an invalid game_id, the message will be "invalid_game".

If a player attempts to make a move in a game they are not in, or they do not provide the correct token, the message will be "not_allowed".

If a player attempts to make an invalid move, the message will be "invalid_move"

### event: opp_disconnect

Response syntax:

```json
{
	"event": "opp_disconnect"
}
```
