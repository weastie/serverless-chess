import json
import boto3
import os
import random

WHITE_LEGAL_MOVES = ['a3', 'a4', 'b3', 'b4', 'c3', 'c4', 'd3', 'd4', 'e3', 'e4', 'f3', 'f4', 'g3', 'g4', 'h3', 'h4', 'Na3', 'Nc3', 'Nf3', 'Nh3']
# BLACK_LEGAL_MOVES = ['a6', 'a5', 'b6', 'b5', 'c6', 'c5', 'd6', 'd5', 'e6', 'e5', 'f6', 'f5', 'g6', 'g5', 'h6', 'h5', 'Na6', 'Nc6', 'Nf6', 'Nh6']

def lambda_handler(event, context):
    api_client = boto3.client('apigatewaymanagementapi', endpoint_url=os.environ['API_URL'])
    connection_id = event['requestContext']['connectionId']
    body = json.loads(event['body'])

    # Build out the game
    white_token = random.randrange(int('10000000', 32), int('vvvvvvvv', 32))
    black_token = random.randrange(int('10000000', 32), int('vvvvvvvv', 32))
    new_game = {
        'game_id': random.randrange(int('10000000', 32), int('vvvvvvvv', 32)),
        'active': True,
        'moves': [],
        'legal_moves': WHITE_LEGAL_MOVES,
        'white_token': white_token,
        'black_token': black_token
        # 'time': {
        #     'remaining_white': 1000*60*5,
        #     'remaining_black': 1000*60*5,
        #     'initial': 1000*60*5,
        #     'increment': 3,
        #     'lastMove': 0, # Time of the previous move
        #     'firstMove': 0 # Time of first move
        # }
    }

    # Random chance if the creator is white or black
    is_white = random.choice([True, False])
    if is_white:
        new_game['white_player'] = connection_id
        new_game['white_name'] = str(body['nickname']) if len(str(body['nickname'])) < 15 else 'Anonymous1'
        new_game['black_name'] = ''
        new_game['black_player'] = ''
    else:
        new_game['white_player'] = ''
        new_game['white_name'] = ''
        new_game['black_name'] = str(body['nickname']) if len(str(body['nickname'])) < 15 else 'Anonymous2'
        new_game['black_player'] = connection_id


    table = boto3.resource('dynamodb').Table(os.environ['TABLE_NAME'])

    table.put_item(Item=new_game)

    # Hide token and connection id
    new_game.pop('white_player')
    new_game.pop('black_player')
    if is_white:
        new_game.pop('black_token')
    else:
        new_game.pop('white_token')

    return {
        'body': json.dumps({'event': 'game_created', 'game': new_game, 'legal_moves': WHITE_LEGAL_MOVES if is_white else []})
    }
