import simplejson as json
import boto3
import os
import random

# WHITE_LEGAL_MOVES = ['a3', 'a4', 'b3', 'b4', 'c3', 'c4', 'd3', 'd4', 'e3', 'e4', 'f3', 'f4', 'g3', 'g4', 'h3', 'h4', 'Na3', 'Nc3', 'Nf3', 'Nh3']
# BLACK_LEGAL_MOVES = ['a6', 'a5', 'b6', 'b5', 'c6', 'c5', 'd6', 'd5', 'e6', 'e5', 'f6', 'f5', 'g6', 'g5', 'h6', 'h5', 'Na6', 'Nc6', 'Nf6', 'Nh6']

def lambda_handler(event, context):
    api_client = boto3.client('apigatewaymanagementapi', endpoint_url=os.environ['API_URL'])
    connection_id = event['requestContext']['connectionId']

    body = json.loads(event['body'])

    table = boto3.resource('dynamodb').Table(os.environ['TABLE_NAME'])


    out = table.get_item(Key={'game_id': int(body['game_id'])})

    if 'Item' in out:
        game = out['Item']

        is_white = False

        nickname = ''

        if game['active'] and ('token' in body or (len(game['moves']) == 0 and game['white_player'] == '' or game['black_player'] == '')):
            if 'token' in body:
                # Player is attempting to rejoin a game
                if game['white_token'] == body['token']:
                    is_white = True
                    nickname = game['white_name']
                elif game['black_token'] == body['token']:
                    is_white = False
                    nickname = game['black_name']
                else:
                    return json.dumps({'event': 'join_error', 'message': 'invalid_token'})
            else:
                nickname = body['nickname']
                if game['white_player'] == '':
                    is_white = True
                else:
                    is_white = False
            if is_white:
                nickname = nickname if len(nickname) <= 15 else 'Anonymous1'
                table.update_item(
                    Key={
                        'game_id': int(body['game_id'])
                    },
                    UpdateExpression='set #wp = :id, #wn = :nick',
                    ExpressionAttributeNames={
                        '#wp': 'white_player',
                        '#wn': 'white_name'
                    },
                    ExpressionAttributeValues={
                        ':id': connection_id,
                        ':nick': nickname
                    }
                )
                # Reflect changes locally
                game['white_name'] = nickname
            else:
                nickname = nickname if len(nickname) <= 15 else 'Anonymous2'
                table.update_item(
                    Key={
                        'game_id': int(body['game_id'])
                    },
                    UpdateExpression='set #bp = :id, #bn = :nick',
                    ExpressionAttributeNames={
                        '#bp': 'black_player',
                        '#bn': 'black_name'
                    },
                    ExpressionAttributeValues={
                        ':id': connection_id,
                        ':nick': nickname
                    }
                )
                # Reflect changes locally
                game['black_name'] = nickname

            # Inform the other player
            if is_white and len(game['black_player']) > 0:
                api_client.post_to_connection(
                    ConnectionId=game['black_player'],
                    Data=json.dumps({'event': 'opp_join', 'opp_name': nickname})
                )
            elif not is_white and len(game['white_player']) > 0:
                api_client.post_to_connection(
                    ConnectionId=game['white_player'],
                    Data=json.dumps({'event': 'opp_join', 'opp_name': nickname})
                )

            # Hide token and connection ID
            game.pop('white_player')
            game.pop('black_player')
            if is_white:
                game.pop('black_token')
            else:
                game.pop('white_token')
            return {
                'body': json.dumps({'event': 'game_join', 'game': game, 'is_white': is_white, 'legal_moves': game['legal_moves'] if not (is_white ^ (len(game['moves']) % 2 == 0)) else []})
            }
        else:
            return {
                'body': json.dumps({'event': 'join_error', 'message': 'game_full'})
            }
    else:
        return {
            'body': json.dumps({'event': 'join_error', 'message': 'game_not_found'})
        }
    # api_client.post_to_connection(ConnectionId=connection_id, Data=json.dumps(new_game))

    # return None
