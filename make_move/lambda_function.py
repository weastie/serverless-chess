import json
import boto3
import os
from weast_chess import *

def lambda_handler(event, context):
    api_client = boto3.client('apigatewaymanagementapi', endpoint_url=os.environ['API_URL'])
    connection_id = event['requestContext']['connectionId']

    body = json.loads(event['body'])

    table = boto3.resource('dynamodb').Table(os.environ['TABLE_NAME'])

    out = table.get_item(Key={'game_id': int(body['game_id'])})

    if 'Item' in out:
        game = out['Item']
        # If the game is active, and the current turn belongs to the player
        if game['active']:
            if (len(game['moves']) % 2 == 0 and game['white_token'] == body['token']) or (len(game['moves']) % 2 == 1 and game['black_token'] == body['token']):
                # Player is allowed to move, check if the move is in legal moves
                move_played = body['move']
                if move_played in game['legal_moves']:
                    # Move is valid, now let's check for new moves
                    game_board = ChessBoard()

                    # Play all the moves up to this point
                    for move in game['moves'] + [move_played]:
                        if move[-1] == '+':
                            move = move[0:-1]
                        if not game_board.player_move(move):
                            return {
                                'body': json.dumps({'event': 'move_error', 'error': 'game corrupted'})
                            }

                    opponent_in_check = game_board.in_check((-1, -1), game_board.turn)

                    # Gather legal moves available for next turn
                    legal_moves = game_board.get_legal_moves()
                    legal_moves.sort()

                    if opponent_in_check:
                        if len(legal_moves) == 0:
                            move_played += '#'
                        else:
                            move_played += '+'
                    elif len(legal_moves) == 0:
                        move_played += '='

                    if move_played[-1] in ['#', '=']:
                        # Game over
                        table.update_item(
                            Key={
                                'game_id': int(body['game_id'])
                            },
                            UpdateExpression='set #active = :false',
                            ExpressionAttributeNames={
                                '#active': 'active'
                            },
                            ExpressionAttributeValues={
                                ':false': False
                            }
                        )
                    # Update player move
                    table.update_item(
                        Key={
                            'game_id': int(body['game_id'])
                        },
                        UpdateExpression='set #moves = list_append(#moves, :move), #legal_moves = :legal_moves',
                        ExpressionAttributeNames={
                            '#moves': 'moves',
                            '#legal_moves': 'legal_moves'
                        },
                        ExpressionAttributeValues={
                            ':move': [move_played],
                            ':legal_moves': legal_moves
                        }
                    )
                    # Send to opponent that move was made, and inform them of next legal moves
                    if len(game['moves']) % 2 == 1 and len(game['white_player']) > 0:
                        api_client.post_to_connection(
                            ConnectionId=game['white_player'],
                            Data=json.dumps({'event': 'move', 'move': move_played, 'legal_moves': legal_moves})
                        )
                    elif len(game['moves']) % 2 == 0 and len(game['black_player']) > 0:
                        api_client.post_to_connection(
                            ConnectionId=game['black_player'],
                            Data=json.dumps({'event': 'move', 'move': move_played, 'legal_moves': legal_moves})
                        )

                    # Confirm to the player that the move was made
                    return {
                        'body': json.dumps({'event': 'move', 'move': move_played, 'legal_moves': []})
                    }
                else:
                    return {
                        'body': json.dumps({'event': 'move_error', 'message': 'invalid_move'})
                    }

            else:
                return {
                    'body': json.dumps({'event': 'move_error', 'message': 'not_allowed'})
                }

    else:
        return {
            'body': json.dumps({'event': 'move_error', 'message': 'invalid_game'})
        }
