import random
import copy
import re
from itertools import permutations

COLOR_WHITE = 0
COLOR_BLACK = 1

PIECE_PAWN = 0
PIECE_KNIGHT = 1
PIECE_BISHOP = 2
PIECE_ROOK = 3
PIECE_QUEEN = 4
PIECE_KING = 5

DIAG_MOVES = [(1, 1), (1, -1), (-1, -1), (-1, 1)]
LINE_MOVES = [(1, 0), (-1, 0), (0, 1), (0, -1)]
KNIGHT_MOVES = [(2, 1), (1, 2), (-1, 2), (-2, 1), (-1, -2), (-2, -1), (1, -2), (2, -1)]


# File to index
def f2i (file):
    return 'abcdefgh'.index(file)

def i2f (index):
    return 'abcdefgh'[index]

# Rank to index
def r2i(n):
    return n - 1

# Index to rank
def i2r(n):
    return n + 1

# Returns list of all possible promotions, For example, input c8 responds [c8=Q,c8=R,c8=B,c8=N]
def promotion(move):
    return [move + '=' + x for x in ['Q','R','B','N']]

def code_to_piece(code):
    return ['', 'N', 'B', 'R', 'Q', 'K'].index(code)

# Bounds checking
def bounds(ranki, file):
    return ranki >= r2i(1) and ranki <= r2i(8) and file >= f2i('a') and file <= f2i('h')

def board_backup(board):
    backup = []
    for i in board:
        row = []
        for j in i:
            if j == False:
                row.append(False)
            else:
                row.append(ChessPiece(j.piece, j.color))
        backup.append(row)
    return backup


class PieceMove:
    def __init__(self, code, start, end, is_capture):
        self.code = code
        self.start = start
        self.end = end
        self.is_capture = is_capture

class ChessPiece:
    def __init__(self, piece, color):
        self.piece = piece
        self.color = color

    def __eq__(self, other):
        if other == False:
            return False

        return self.piece == other.piece and self.color == other.color

    def __neq__(self, other):
        return not self == other

    def __bool__(self):
        return True

class ChessBoard:
    # board[0] is the 1st rank
    # board[0][0] is a1
    # board[7] is the 8th rank
    # board[7][7] is h8

    def __init__(self):
        self.board = []
        # Note: castle rights being True do not guarantee you can castle, they must be true and there must be a rook and no squares in check
        self.castle_rights = [[True, True], [True, True]] # [[Long casttle for white, short castle for white], [Same for black...]]
        self.turn = COLOR_WHITE

        self.possible_ep = -1 # If a pawn moved two spots last turn, this value will equal its file

        for i in range(0, 8):
            self.board.append([False] * 8)

        self.board[0] = [
            ChessPiece(PIECE_ROOK, COLOR_WHITE),
            ChessPiece(PIECE_KNIGHT, COLOR_WHITE),
            ChessPiece(PIECE_BISHOP, COLOR_WHITE),
            ChessPiece(PIECE_QUEEN, COLOR_WHITE),
            ChessPiece(PIECE_KING, COLOR_WHITE),
            ChessPiece(PIECE_BISHOP, COLOR_WHITE),
            ChessPiece(PIECE_KNIGHT, COLOR_WHITE),
            ChessPiece(PIECE_ROOK, COLOR_WHITE),
        ]
        self.board[1] = [ChessPiece(PIECE_PAWN, COLOR_WHITE)] * 8
        self.board[6] = [ChessPiece(PIECE_PAWN, COLOR_BLACK)] * 8
        self.board[7] = [
            ChessPiece(PIECE_ROOK, COLOR_BLACK),
            ChessPiece(PIECE_KNIGHT, COLOR_BLACK),
            ChessPiece(PIECE_BISHOP, COLOR_BLACK),
            ChessPiece(PIECE_QUEEN, COLOR_BLACK),
            ChessPiece(PIECE_KING, COLOR_BLACK),
            ChessPiece(PIECE_BISHOP, COLOR_BLACK),
            ChessPiece(PIECE_KNIGHT, COLOR_BLACK),
            ChessPiece(PIECE_ROOK, COLOR_BLACK),
        ]

    def player_move(self, move):
        if self.make_move(move):
            self.turn = (self.turn + 1) % 2
            return True
        return False

    # Returns list of legal moves
    # Todo: Take into account pins (check if in check after move)
    # Todo: en passent (google it)
    # Todo: promotion (pawn captures with promotion)
    def get_legal_moves(self):
        legal_moves = []
        # We find the location of the king to make it quick to find if you are in check
        king_location = (-1, -1)
        # Note that the king location would change if the player is moving the king, but stay the same otherwise

        # Pawns moves are too specific to what color, so we run them in a separate loop
        # Pawn moves are never ambigous, i.e, never have to be more specific than e4 or exd4
        if self.turn == COLOR_WHITE:
            # Pawn moves for white

            # First, check for en passent
            if self.possible_ep >= 0:
                if bounds(r2i(5), self.possible_ep - 1):
                    if self.board[r2i(5)][self.possible_ep - 1] == ChessPiece(PIECE_PAWN, COLOR_WHITE):
                        # It's valid
                        legal_moves.append('{}x{}6'.format(i2f(self.possible_ep - 1), i2f(self.possible_ep)))
                if bounds(r2i(5), self.possible_ep + 1):
                    if self.board[r2i(5)][self.possible_ep + 1] == ChessPiece(PIECE_PAWN, COLOR_WHITE):
                        # It's valid
                        legal_moves.append('{}x{}6'.format(i2f(self.possible_ep + 1), i2f(self.possible_ep)))

            # Check for all other pawn moves
            for ranki in range(8):
                for file in range(8):
                    if self.board[ranki][file] == ChessPiece(PIECE_PAWN, COLOR_WHITE):
                        # Can this pawn move forward?
                        # Move forward two: must be on 2nd rank and nothing on 3rd/4th rank
                        if ranki == r2i(2):
                            if self.board[r2i(4)][file] == False and self.board[r2i(3)][file] == False:
                                legal_moves.append(i2f(file) + '4')
                        # Standard pawn push
                        if self.board[ranki+1][file] == False:
                            base_move = i2f(file) + str(i2r(ranki+1))
                            if ranki == r2i(7):
                                legal_moves += [m for m in promotion(base_move)]
                            else:
                                legal_moves.append(base_move)
                        # Can this pawn capture something?
                        if file > f2i('a') and self.board[ranki+1][file-1] and self.board[ranki+1][file-1].color == COLOR_BLACK:
                            base_move = '{}x{}{}'.format(i2f(file), i2f(file-1), i2r(ranki+1))
                            if ranki == r2i(7):
                                legal_moves += [m for m in promotion(base_move)]
                            else:
                                legal_moves.append(base_move)
                        if file < f2i('h') and self.board[ranki+1][file+1] and self.board[ranki+1][file+1].color == COLOR_BLACK:
                            base_move = '{}x{}{}'.format(i2f(file), i2f(file+1), i2r(ranki+1))
                            if ranki == r2i(7):
                                legal_moves += [m for m in promotion(base_move)]
                            else:
                                legal_moves.append(base_move)
        elif self.turn == COLOR_BLACK:
            # Pawn moves for black

            # First, check for en passent
            if self.possible_ep >= 0:
                if bounds(r2i(4), self.possible_ep - 1):
                    if self.board[r2i(4)][self.possible_ep - 1] == ChessPiece(PIECE_PAWN, COLOR_BLACK):
                        # It's valid
                        legal_moves.append('{}x{}3'.format(i2f(self.possible_ep - 1), i2f(self.possible_ep)))
                if bounds(r2i(4), self.possible_ep + 1):
                    if self.board[r2i(4)][self.possible_ep + 1] == ChessPiece(PIECE_PAWN, COLOR_BLACK):
                        # It's valid
                        legal_moves.append('{}x{}3'.format(i2f(self.possible_ep + 1), i2f(self.possible_ep)))

            # Check for all other pawn moves
            for ranki in range(8):
                for file in range(8):
                    if self.board[ranki][file] == ChessPiece(PIECE_PAWN, COLOR_BLACK):
                        # Move forward two: must be on 7th rank and nothing on 6th/5th rank
                        if ranki == r2i(7):
                            if self.board[r2i(5)][file] == False and self.board[r2i(6)][file] == False:
                                legal_moves.append(i2f(file) + '5')
                        # Standard pawn push
                        if self.board[ranki-1][file] == False:
                            base_move = i2f(file) + str(i2r(ranki-1))
                            if ranki == r2i(2):
                                legal_moves += [m for m in promotion(base_move)]
                            else:
                                legal_moves.append(base_move)

                        # Can this pawn capture something?
                        if file > f2i('a') and self.board[ranki-1][file-1] and self.board[ranki-1][file-1].color == COLOR_WHITE:
                            base_move = '{}x{}{}'.format(i2f(file), i2f(file-1), i2r(ranki-1))
                            if ranki == r2i(2):
                                legal_moves += [m for m in promotion(base_move)]
                            else:
                                legal_moves.append(base_move)
                        if file < f2i('h') and self.board[ranki-1][file+1] and self.board[ranki-1][file+1].color == COLOR_WHITE:
                            base_move = '{}x{}{}'.format(i2f(file), i2f(file+1), i2r(ranki-1))
                            if ranki == r2i(2):
                                legal_moves += [m for m in promotion(base_move)]
                            else:
                                legal_moves.append(base_move)

        # Don't check for castling rights if the king is in check
        c_ranki = self.turn * 7
        if (self.castle_rights[self.turn][0] or self.castle_rights[self.turn][1]) and not self.in_check((c_ranki, f2i('e')), self.turn):
            # Check for short castling rights
            if self.castle_rights[self.turn][0]:
                # Check for rook and check for no pieces in the way
                if self.board[c_ranki][f2i('h')] == ChessPiece(PIECE_ROOK, self.turn) and not (self.board[c_ranki][f2i('f')] or self.board[c_ranki][f2i('g')]):
                    # Make sure the king does not pass through check
                    if not self.in_check((c_ranki, f2i('f')), self.turn):
                        legal_moves.append('O-O')

            # Check for long castling rights
            if self.castle_rights[self.turn][1]:
                # Check for rook and check for no pieces in the way
                if self.board[c_ranki][f2i('a')] == ChessPiece(PIECE_ROOK, self.turn) and not (self.board[c_ranki][f2i('b')] or self.board[c_ranki][f2i('c')] or self.board[c_ranki][f2i('d')]):
                    # Make sure the king does not pass through check
                    if not self.in_check((c_ranki, f2i('d')), self.turn):
                        legal_moves.append('O-O-O')

        # For the rest of pieces, we have to take into account some edge cases
        # Unlike pawns, with other pieces we can have a situation where multiple can go to the same destination square
        # Therefore, instead of direcltly adding moves to legal_moves, we add them to a different list for later processing
        piece_moves = []
        for ranki in range(8):
            for file in range(8):
                if self.board[ranki][file] == ChessPiece(PIECE_KNIGHT, self.turn):
                    # Knights have 8 moves, permutate them all
                    for k_move in KNIGHT_MOVES:
                        # Bounds checking rank
                        if bounds(ranki + k_move[0], file + k_move[1]):
                            if self.board[ranki + k_move[0]][file + k_move[1]] == False:
                                # If there is no piece there, knight can move there
                                piece_moves.append(PieceMove('N', (ranki, file), (ranki + k_move[0], file + k_move[1]), False))
                            elif self.board[ranki + k_move[0]][file + k_move[1]].color != self.turn:
                                # If there is piece of opposite color, knight can take
                                piece_moves.append(PieceMove('N', (ranki, file), (ranki + k_move[0], file + k_move[1]), True))
                elif self.board[ranki][file] and self.board[ranki][file].color == self.turn and self.board[ranki][file].piece in [PIECE_ROOK, PIECE_BISHOP, PIECE_QUEEN]:
                    directions = []
                    p_code = ''
                    if self.board[ranki][file].piece in [PIECE_BISHOP, PIECE_QUEEN]:
                        directions += DIAG_MOVES
                        p_code = 'B'
                    if self.board[ranki][file].piece in [PIECE_ROOK, PIECE_QUEEN]:
                        p_code = 'Q' if p_code == 'B' else 'R'
                        directions += LINE_MOVES

                    for d in directions:
                        dr = d[0] # delta rank
                        df = d[1] # delta file
                        m = 1
                        while bounds(ranki + dr * m, file + df * m):
                            if not self.board[ranki + dr * m][file + df * m]:
                                piece_moves.append(PieceMove(p_code, (ranki, file), (ranki + dr * m, file + df * m), False)) # Not capture
                            elif self.board[ranki + dr * m][file + df * m].color == self.turn:
                                break
                            else:
                                piece_moves.append(PieceMove(p_code, (ranki, file), (ranki + dr * m, file + df * m), True)) # Capture
                                break
                            m += 1
                elif self.board[ranki][file] == ChessPiece(PIECE_KING, self.turn):
                    king_location = (ranki, file)
                    # King is similar to queen but we don't worry about m
                    directions = DIAG_MOVES + LINE_MOVES
                    for d in directions:
                        if bounds(ranki + d[0], file + d[1]):
                            if not self.board[ranki + d[0]][file + d[1]]:
                                piece_moves.append(PieceMove('K', (ranki, file), (ranki + d[0], file + d[1]), False))
                            elif self.board[ranki + d[0]][file + d[1]].color != self.turn:
                                piece_moves.append(PieceMove('K', (ranki, file), (ranki + d[0], file + d[1]), True))

        # Process piece moves
        while len(piece_moves) > 0:
            # Pop first move
            move = piece_moves.pop(0)
            # Keep track of moves with same end destination and same piece code
            same_dest = []
            i = 1
            while i < len(piece_moves):
                if piece_moves[i].code == move.code and piece_moves[i].start != move.start and piece_moves[i].end == move.end:
                    same_dest.append(piece_moves.pop(i))
                else:
                    i += 1
            if len(same_dest) >= 2:
                # Must specify start rank and file
                same_dest.append(move)
                for m in same_dest:
                    legal_moves.append('{}{}{}{}{}{}'.format(m.code, i2f(m.start[1]), i2r(m.start[0]), 'x' if m.is_capture else '', i2f(m.end[1]), i2r(m.end[0])))
            elif len(same_dest) == 1:
                # Must specify only rank or file (file takes precedence)
                if move.start[1] != same_dest[0].start[1]:
                    # They have different files, use that
                    legal_moves.append('{}{}{}{}{}'.format(move.code, i2f(move.start[1]), 'x' if move.is_capture else '', i2f(move.end[1]), i2r(move.end[0])))
                    legal_moves.append('{}{}{}{}{}'.format(move.code, i2f(same_dest[0].start[1]), 'x' if move.is_capture else '', i2f(move.end[1]), i2r(move.end[0])))
                else:
                    # Must use different ranks
                    legal_moves.append('{}{}{}{}{}'.format(move.code, i2r(move.start[0]), 'x' if move.is_capture else '', i2f(move.end[1]), i2r(move.end[0])))
                    legal_moves.append('{}{}{}{}{}'.format(move.code, i2r(same_dest[0].start[0]), 'x' if move.is_capture else '', i2f(move.end[1]), i2r(move.end[0])))
            else:
                legal_moves.append('{}{}{}{}'.format(move.code, 'x' if move.is_capture else '', i2f(move.end[1]), i2r(move.end[0])))
        # print(legal_moves) # TODO: REMOVE THIS

        # Post processing of legal_moves, check if any of them result in self check
        actual_legal_moves = []
        backup_board = board_backup(self.board)
        for move in legal_moves:
            # Check if a move results in check. If the king is moved, we need to update the king location
            self.make_move(move)
            if move[0] == 'K':
                temp_king_loc = (r2i(int(move[-1])), f2i(move[-2]))
                if not self.in_check(temp_king_loc, self.turn):
                    actual_legal_moves.append(move)
            elif move in ['O-O', 'O-O-O']:
                temp_king_loc = (c_ranki, f2i('g')) if move == 'O-O' else (c_ranki, f2i('c'))
                if not self.in_check(temp_king_loc, self.turn):
                    actual_legal_moves.append(move)
            else:
                if not self.in_check(king_location, self.turn):
                    actual_legal_moves.append(move)
            self.board = board_backup(backup_board)

        return actual_legal_moves

    # Does not determine if move is legal or not, just makes it
    def make_move(self, move):
        # Reset en passent
        self.possible_ep = -1

        if re.match('^[a-h][1-8](=[RNBQ])?$', move):
            # Pawn push
            file = f2i(move[0])
            ranki = r2i(int(move[1]))

            if self.turn == COLOR_WHITE:
                # Check for special case with two push forward
                if ranki == r2i(4) and self.board[r2i(3)][file] != ChessPiece(PIECE_PAWN, COLOR_WHITE):
                    self.board[r2i(2)][file] = False
                    self.board[r2i(4)][file] = ChessPiece(PIECE_PAWN, COLOR_WHITE)
                    self.possible_ep = file
                    return True

                # Push pawn one forward
                self.board[ranki-1][file] = False
                if ranki == r2i(8): # Promotion
                    self.board[ranki][file] = ChessPiece(code_to_piece(move[-1]), COLOR_WHITE)
                else:
                    self.board[ranki][file] = ChessPiece(PIECE_PAWN, COLOR_WHITE)
                return True

            if self.turn == COLOR_BLACK:
                #  Pawn can move 2 spaces if COLOR_BLACK moves to rank 4
                if ranki == r2i(5) and self.board[r2i(6)][file] != ChessPiece(PIECE_PAWN, COLOR_BLACK):
                    self.board[r2i(7)][file] = False
                    self.board[r2i(5)][file] = ChessPiece(PIECE_PAWN, COLOR_BLACK)
                    self.possible_ep = file
                    return True

                # Push pawn one forward
                self.board[ranki+1][file] = False
                if ranki == r2i(1): # Promotion
                    self.board[ranki][file] = ChessPiece(code_to_piece(move[-1]), COLOR_BLACK)
                else:
                    self.board[ranki][file] = ChessPiece(PIECE_PAWN, COLOR_BLACK)
                return True
        elif re.match('^[a-h]x[a-h][1-8](=[RNBQ])?$', move):
            # Basic pawn capture
            init_file = f2i(move[0])
            end_file = f2i(move[2])
            end_ranki = r2i(int(move[3]))
            end_piece = PIECE_PAWN

            if self.turn == COLOR_WHITE:
                if end_ranki == r2i(6) and not self.board[end_ranki][end_file]:
                    # Check for en passent capture
                    self.board[end_ranki - 1][end_file] = False
                # Remove inital pawn position
                self.board[end_ranki - 1][init_file] = False
                if end_ranki == r2i(8): # Promotion
                    end_piece = code_to_piece(move[-1])
            else:
                if end_ranki == r2i(3) and not self.board[end_ranki][end_file]:
                    # Check for en passent capture
                    self.board[end_ranki+1][end_file] = False
                # Remove initial pawn position
                self.board[end_ranki+1][init_file] = False
                if end_ranki == r2i(1): # Promotion
                    end_piece = code_to_piece(move[-1])

            self.board[end_ranki][end_file] = ChessPiece(end_piece, self.turn)
            return True
        elif re.match('^N([a-h])?([1-8])?(x)?[a-h][1-8]$', move):
            # Knight move or capture (they are kinda the same)
            end_file = f2i(move[-2])
            end_ranki = r2i(int(move[-1]))
            init_file = -1
            init_ranki = -1

            # Determine if an initial spot is mentioned
            move_without_capture = move.replace('x', '')
            if len(move_without_capture) == 4:
                if move[1] in 'abcdefgh':
                    init_file = f2i(move[1])
                else:
                    init_ranki = r2i(int(move[1]))

            if len(move_without_capture) == 5:
                init_file = f2i(move[1])
                init_ranki = r2i(int(move[2]))

            # If not specified what file or rank, we must find them
            if init_file == -1 or init_ranki == -1:
                for k_move in KNIGHT_MOVES:
                    if bounds(end_ranki + k_move[0], end_file + k_move[1]):
                        # Basically, if one of init_file or init_rank are specified, it must match
                        if (init_file in [-1, end_file + k_move[1]]) and (init_ranki in [-1, end_ranki + k_move[0]]):
                            if self.board[end_ranki + k_move[0]][end_file + k_move[1]] == ChessPiece(PIECE_KNIGHT, self.turn):
                                init_file = end_file + k_move[1]
                                init_ranki = end_ranki + k_move[0]
            # Bad move
            if init_file == -1 or init_ranki == -1:
                return False
            # Put knight at the end position
            self.board[end_ranki][end_file] = ChessPiece(PIECE_KNIGHT, self.turn)
            # Clear the init position
            self.board[init_ranki][init_file] = False
            return True
        elif re.match('^([BQR])([a-h])?([1-8])?(x)?[a-h][1-8]$', move):
            # Bishop, rook, or queen move or capture
            end_file = f2i(move[-2])
            end_ranki = r2i(int(move[-1]))
            init_file = -1
            init_ranki = -1

            # Determine if an initial spot is mentioned
            move_without_capture = move.replace('x', '')
            if len(move_without_capture) == 4:
                if move[1] in 'abcdefgh':
                    init_file = f2i(move[1])
                else:
                    init_ranki = r2i(int(move[1]))

            if len(move_without_capture) == 5:
                init_file = f2i(move[1])
                init_ranki = r2i(int(move[2]))

            # We must do some searching if start and end position are not specified
            if init_file == -1 or init_ranki == -1:
                directions = []
                p_index = -1
                if move[0] in ['B', 'Q']:
                    directions += DIAG_MOVES
                    p_index = PIECE_BISHOP
                if move[0] in ['R', 'Q']:
                    directions += LINE_MOVES
                    p_index = PIECE_QUEEN if p_index == PIECE_BISHOP else PIECE_ROOK

                # If not specified what file or rank, we must find it by searching from the end position
                for d in directions:
                    dr = d[0] # delta rank
                    df = d[1] # delta file
                    m = 1
                    while bounds(end_ranki + dr * m, end_file + df * m):
                        # If one of initial file or initial rank were specified, make sure it matches
                        if (init_file in [-1, end_file + df * m]) and (init_ranki in [-1, end_ranki + dr * m]):
                            if self.board[end_ranki + dr * m][end_file + df * m]:
                                if self.board[end_ranki + dr * m][end_file + df * m] == ChessPiece(p_index, self.turn):
                                    init_file = end_file + df * m
                                    init_ranki = end_ranki + dr * m
                                break
                        m += 1
                    if init_file != -1 and init_ranki != -1:
                        break

            if init_file == -1 or init_ranki == -1:
                return False

            # If a rook is moved, be sure to remove castling rights
            if move[0] == 'R' and init_file == f2i('h') and init_ranki in [r2i(1), r2i(8)]:
                self.castle_rights[self.turn][0] = False # No short castle
            elif move[0] == 'R' and init_file == f2i('a') and init_ranki in [r2i(1), r2i(8)]:
                self.castle_rights[self.turn][1] = False # No long castle

            # Put piece at the end position
            self.board[end_ranki][end_file] = ChessPiece(p_index, self.turn)
            # Clear the init position
            self.board[init_ranki][init_file] = False
            return True
        elif re.match('^K(x)?[a-h][1-8]$', move):
            end_file = f2i(move[-2])
            end_ranki = r2i(int(move[-1]))
            init_file = -1
            init_ranki = -1

            # Find where the king was initially
            directions = DIAG_MOVES + LINE_MOVES
            for d in directions:
                if bounds(end_ranki + d[0], end_file + d[1]):
                    if self.board[end_ranki + d[0]][end_file + d[1]] == ChessPiece(PIECE_KING, self.turn):
                        init_file = end_file + d[1]
                        init_ranki = end_ranki + d[0]
                        break

            if init_file == -1 or init_ranki == -1:
                return False

            # Remove castle rights
            self.castle_rights[self.turn] = [False, False]

            # Put king at the end position
            self.board[end_ranki][end_file] = ChessPiece(PIECE_KING, self.turn)
            # Clear the init position
            self.board[init_ranki][init_file] = False
            return True
        elif move == 'O-O':
            c_ranki = self.turn * 7
            # Obviously now that they have castled they should lose castling rights
            self.castle_rights[self.turn] = [False, False]
            # Move king
            self.board[c_ranki][f2i('e')] = False
            self.board[c_ranki][f2i('g')] = ChessPiece(PIECE_KING, self.turn)
            # Move rook
            self.board[c_ranki][f2i('h')] = False
            self.board[c_ranki][f2i('f')] = ChessPiece(PIECE_ROOK, self.turn)
            return True
        elif move == 'O-O-O':
            c_ranki = self.turn * 7
            # Obviously now that they have castled they should lose castling rights
            self.castle_rights[self.turn] = [False, False]
            # Move king
            self.board[c_ranki][f2i('e')] = False
            self.board[c_ranki][f2i('c')] = ChessPiece(PIECE_KING, self.turn)
            # Move rook
            self.board[c_ranki][f2i('a')] = False
            self.board[c_ranki][f2i('d')] = ChessPiece(PIECE_ROOK, self.turn)
            return True
        else:
            return False

    # Determines whether or not the board is currently in check, provide king location to make it faster
    def in_check(self, kloc, color):
        if kloc[0] == -1:
            # Must find the king
            for ranki in range(8):
                for file in range(8):
                    if self.board[ranki][file] == ChessPiece(PIECE_KING, color):
                        kloc = (ranki, file)


        # To make this function fast, first find the easy things, like, a pawn or knight
        if color == COLOR_WHITE:
            # Check for pawn checks
            if bounds(kloc[0] + 1, kloc[1] + 1) and self.board[kloc[0] + 1][kloc[1] + 1] == ChessPiece(PIECE_PAWN, COLOR_BLACK):
                return True
            elif bounds(kloc[0] + 1, kloc[1] - 1) and self.board[kloc[0] + 1][kloc[1] - 1] == ChessPiece(PIECE_PAWN, COLOR_BLACK):
                return True
        elif color == COLOR_BLACK:
            # Check for pawn checks
            if bounds(kloc[0] - 1, kloc[1] + 1) and self.board[kloc[0] - 1][kloc[1] + 1] == ChessPiece(PIECE_PAWN, COLOR_WHITE):
                return True
            elif bounds(kloc[0] - 1, kloc[1] - 1) and self.board[kloc[0] - 1][kloc[1] - 1] == ChessPiece(PIECE_PAWN, COLOR_WHITE):
                return True

        # Check knight moves
        for k_move in KNIGHT_MOVES:
            if bounds(kloc[0] + k_move[0], kloc[1] + k_move[1]):
                if self.board[kloc[0] + k_move[0]][kloc[1] + k_move[1]] == ChessPiece(PIECE_KNIGHT, (color + 1) % 2):
                    return True

        # Check diagonals
        for d in DIAG_MOVES:
            dr = d[0]
            df = d[1]
            m = 1
            while bounds(kloc[0] + dr * m, kloc[1] + df * m):
                # same piece
                if self.board[kloc[0] + dr * m][kloc[1] + df * m]:
                    if self.board[kloc[0] + dr * m][kloc[1] + df * m].color == color:
                        break
                    elif self.board[kloc[0] + dr * m][kloc[1] + df * m].piece in [PIECE_BISHOP, PIECE_QUEEN]:
                        return True
                    elif m == 1 and self.board[kloc[0] + dr * m][kloc[1] + df * m].piece == PIECE_KING:
                        return True
                m += 1

        # Check lines
        for d in LINE_MOVES:
            dr = d[0]
            df = d[1]
            m = 1
            while bounds(kloc[0] + dr * m, kloc[1] + df * m):
                # same piece
                if self.board[kloc[0] + dr * m][kloc[1] + df * m]:
                    if self.board[kloc[0] + dr * m][kloc[1] + df * m].color == color:
                        break
                    elif self.board[kloc[0] + dr * m][kloc[1] + df * m].piece in [PIECE_ROOK, PIECE_QUEEN]:
                        return True
                    elif m == 1 and self.board[kloc[0] + dr * m][kloc[1] + df * m].piece == PIECE_KING:
                        return True
                m += 1

        return False
