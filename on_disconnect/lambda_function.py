import json
import boto3
import os
from boto3.dynamodb.conditions import Attr

def lambda_handler(event, context):

    # print(event)
    # If a player disconnects, check if they were in an active game
    api_client = boto3.client('apigatewaymanagementapi', endpoint_url=os.environ['API_URL'])
    table = boto3.resource('dynamodb').Table(os.environ['TABLE_NAME'])
    connection_id = event['requestContext']['connectionId']

    games = table.scan(
        FilterExpression=Attr('active').eq(True) & (Attr('white_player').eq(connection_id) | Attr('black_player').eq(connection_id))
    )

    # remove them from active games
    if len(games['Items']) > 0:
        for game in games['Items']:
            is_white = connection_id == game['white_player']

            table.update_item(
                Key={
                    'game_id': game['game_id']
                },
                UpdateExpression='set #player = :none',
                ExpressionAttributeNames={
                    '#player': 'white_player' if is_white else 'black_player'
                },
                ExpressionAttributeValues={
                    ':none': ''
                }
            )
            # Inform other player that someone disconnected, if there is another player
            if (is_white and len(game['black_player']) > 0) or (not is_white and len(game['white_player']) > 0):
                api_client.post_to_connection(
                    ConnectionId=game['black_player'] if is_white else game['white_player'],
                    Data=json.dumps({'event': 'opp_disconnect'})
                )
