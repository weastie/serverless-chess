import json
import boto3
import os

def lambda_handler(event, context):
    api_client = boto3.client('apigatewaymanagementapi', endpoint_url=os.environ['API_URL'])
    connection_id = event['requestContext']['connectionId']
    body = json.loads(event['body'])

    # blab

    table = boto3.resource('dynamodb').Table(os.environ['TABLE_NAME'])

    out = table.get_item(Key={'game_id': int(body['game_id'])})

    if 'Item' in out:
        game = out['Item']

        if game['active'] and body['token'] in [game['black_token'], game['white_token']]:
            is_white = body['token'] == game['white_token']

            if is_white and len(game['black_player']) > 0:
                msg = {'event': 'chat', 'from': game['white_name'], 'message': body['message'][:140]}
                api_client.post_to_connection(
                    ConnectionId=game['black_player'],
                    Data=json.dumps(msg)
                )
                return {
                    'body': json.dumps(msg)
                }
            if not is_white and len(game['white_player']) > 0:
                msg = {'event': 'chat', 'from': game['black_name'], 'message': body['message'][:140]}
                api_client.post_to_connection(
                    ConnectionId=game['white_player'],
                    Data=json.dumps(msg)
                )
                return {
                    'body': json.dumps(msg)
                }
            return {
                'body': json.dumps({'event': 'chat_error', 'message': 'no_one_heard'})
            }

        else:
            return {
                'body': json.dumps({'event': 'chat_error', 'message': 'not_allowed'})
            }
